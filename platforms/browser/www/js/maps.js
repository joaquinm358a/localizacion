var app ={
	model:{
		marcadores:[]
	},
	miMapa: null,
	inicio: function(){
		this.iniciaFastClick();
		//Boton temporal
		console.log(storage.inicio);


		if(app.model.marcadores == undefined){
			app.model.marcadores = []
		}

		
	},
	cargarMarcadores: function(mar){
		app.model.marcadores = mar;
		marcadores = app.model.marcadores;
		for(var i = 0; i<marcadores.lenght;){ 	//No quitar ;
			app.pintaMarcador([
				marcadores[i].latlng,
				marcadores[i].latlng],
				marcadores[i].nombre,
				app.miMapa
				);//latlng, nombre, mapa
		}
	},

	iniciaFastClick: function(){
		FastClick.attach(document.body);
	},
	dispositivoListo: function(){
		console.log(app.model);
		navigator.geolocation.getCurrentPosition(app.dibujaCoords, app.errorCoords);
	},
	dibujaCoords: function(ubicacion){
		 app.miMapa = L.map('mapa').setView([ubicacion.coords.latitude, ubicacion.coords.longitude], 13);
		 miMapa = app.miMapa;
		
		L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/outdoors-v10/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoiam9hcXVpbm0zNTgiLCJhIjoiY2p6bXBhbW9lMGV5dTNjcDdkdGhyczloaiJ9.Ul2et-B4EF5_nQ0hGhdk9Q', {
			attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
			maxZoom: 15,
			minZoom: 2
			}).addTo(app.miMapa);

		L.tileLayer('mapas/BASE/4uMaps/{z}/{x}/{y}.png', {
			attribution: '4uMaps',
			maxZoom: 10,
			minZoom: 2
			}).addTo(app.miMapa);

		L.tileLayer('mapas/4uMaps/{z}/{x}/{y}.png', {
			attribution: '4uMaps',
			maxZoom: 10,
			minZoom: 2
			}).addTo(app.miMapa);


			
		app.pintaMarcador([ubicacion.coords.latitude, ubicacion.coords.longitude], "Estoy aquí",miMapa);
		
		//app.cargarMarcadores();
		
		app.miMapa.on('click',function(evento){              //Es como un addEventListener al evento de click.
			var num = app.model.marcadores.length+1;
			var texto = "Marcador: "+num;
			console.log(texto);
			app.model.marcadores.push({
				nombre: texto,
				latlng: evento.latlng,
			});
			console.log("guardado en model");
			app.pintaMarcador(evento.latlng, texto, app.miMapa);
			app.guardarD();
			


		});
		console.log("on");
	},

	guardarD: function(){
		storage.guardar(app.model);
	},
	errorCoords: function (error){
		alert(error.message);
	},
	pintaMarcador: function(latlng,texto,mapa){
		var marcador = L.marker(latlng).addTo(mapa);
		marcador.bindPopup(texto).openPopup();
		
	},
	
}
if ('addEventListener' in document){
	document.addEventListener('DOMContentLoaded', function(){
		app.inicio();
	}, false);
	document.addEventListener('deviceready', function(){
		app.dispositivoListo();
	},false);
}